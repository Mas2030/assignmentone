package com.Ans1;

import java.util.Scanner;

public class OddEvenCount {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int startingNumber, endingNumber, eventCounter = 0, oddCounter = 0;
        System.out.print("Enter Starting number: ");
        startingNumber = sc.nextInt();
        System.out.print("Enter Last number: ");
        endingNumber = sc.nextInt();

        if (startingNumber < 0 || endingNumber < 0) {
            System.out.println("invalid input");
            throw new IllegalArgumentException("invalid input");
        }
        int evenNumberList[];
        int arrLength = Math.abs((endingNumber - startingNumber) / 2);
        if (arrLength < 0) {
            throw new IllegalArgumentException("invalid input");
        }
        evenNumberList = new int[arrLength + 1];
        for (int i = startingNumber, j = 0; i <= endingNumber; i++) {
            if (i % 2 == 0) {
                eventCounter++;
                evenNumberList[j] = i;
                j++;
            } else if (i % 2 == 1) {
                oddCounter++;
            }
        }
        System.out.println("Total even number count: " + eventCounter);
        System.out.println("Total odd number count: " + oddCounter);

        System.out.printf("The even numbers between %d and %d are: ", startingNumber, endingNumber);

        for (int i = 0; i < evenNumberList.length; i++) {
            System.out.print(evenNumberList[i] + " ");
        }
    }
}
