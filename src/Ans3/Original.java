package Ans3;

public class Original {
    public static void main(String[] args) {
        // assigning value to member variable by constructor
        Student std1 = new Student(1, "Neel", "7", 13);
        System.out.println(std1);
        std1.numberOfStudent = 1;
        Student.numberOfStudent = 2;
        Student std2 = new Student();
        std2.setId(2);
        std2.setName("masrur");
        std2.setClassNumber("3");

        std2.age = 26;
        System.out.println(std2);
    }
}


