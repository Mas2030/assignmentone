package Ans3;

public class Student extends Human {
    private int id;
    private String name;
    private String classNumber;
    public static int numberOfStudent;

    public Student() {
    }

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Student(int id, String name, String classNumber, int age) {
        super(age);
        this.id = id;
        this.name = name;
        this.classNumber = classNumber;
        numberOfStudent++;
    }

    static {
        System.out.println("this is static block");
    }

    {
        System.out.println("this is non static block");
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", Age ='" + age + '\'' +
                ", classNumber='" + classNumber + '\'' +
                ", total Student='" + numberOfStudent + '\'' +
                '}';
    }
}
