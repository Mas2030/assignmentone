package Ans2;

public class Pattern {
    public static void main(String[] args) {
        int rows = 3;
        for (int i = 1; i <= rows; i++) {
            if (i != rows) {
                for (int j = 1; j <= i; j++) {
                    System.out.print("*");
                }
            } else {
                for (int j = 1; j <= i + 1; j++) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }

        for (int i = 1; i <= rows - 1; i++) {
            for (int j = rows - 1; j >= i; j--) {
                System.out.print("*");
            }
            for (int k = 1; k < i; k++) {
                System.out.print(" ");
            }
            System.out.println();
        }

    }
}
